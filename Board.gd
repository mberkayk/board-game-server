extends Node

var labels : Array


var id:int
# Player info, associate player network ID to data
var player_info = { }
var first_player:int = 0
var second_player:int = 0


var players_who_left = []
onready var first_abandoned_timer:Timer = Timer.new()
onready var second_abandoned_timer:Timer = Timer.new()
var game_started:bool = false

enum phase {PREGAME, PREP, BATTLE}
var current_phase:int = phase.PREGAME

class Piece:
	var tile_pos:Vector2
	var piece_type:int
	var color:int
	
	func _init(vec, type, col):
		tile_pos = vec
		piece_type = type
		color = col

var pieces_in_the_game = []

onready var network = get_parent().network

func _ready():
	first_abandoned_timer.wait_time = 3 * 60
	first_abandoned_timer.connect("timeout", self, "first_player_abandoned_game")
	add_child(first_abandoned_timer)
	second_abandoned_timer.wait_time = 3 * 60
	second_abandoned_timer.connect("timeout", self, "second_player_abandoned_game")
	add_child(second_abandoned_timer)

func construct(_id):
	id = _id

func get_next_vacant_spot():
	return player_info.size()

func _player_disconnected(id):
	if(game_started == false):
		print("Disconnected " + str(player_info[id]))
		player_info.erase(id) # Erase player from info
#		update_info_ui()
	else:
		players_who_left.append(player_info[id]["name"])
		if(id == first_player):
			first_abandoned_timer.start()
		elif(id == second_player):
			second_abandoned_timer.start()

func first_player_abandoned_game():
	player_abandoned_game()
	player_info.erase(first_player)

func second_player_abandoned_game():
	player_abandoned_game()
	player_info.erase(second_player)

func player_abandoned_game():
	game_started = false
#	update_info_ui()
	current_phase = phase.PREGAME


func send_board_info(id):
	var current_phase_node:Node
	if(current_phase == phase.PREP):
		current_phase_node = $"Board/Preparation Phase"
	elif(current_phase == phase.BATTLE):
		current_phase_node = $"Board/Battle Phase"
		
	var player_time = 0
	var opponent_time = 0
	if(id == first_player):
		player_time = current_phase_node.first_timer
		opponent_time = current_phase_node.second_timer
	elif(id == second_player):
		player_time = current_phase_node.second_timer
		opponent_time = current_phase_node.first_timer
	
	var tile_pos = []
	var color = []
	var type = [] 
	
	for piece in pieces_in_the_game:
		tile_pos.append(piece.tile_pos)
		color.append(piece.color)
		type.append(piece.piece_type)
	
	network.send_board_info(id, player_info[first_player]["name"],
		Global.colors.YELLOW, player_time, opponent_time, current_phase,
		tile_pos, type, color)


func register_player(id, info):
	#see if someone is coming back after disconnecting
	if(game_started and (info["name"] in players_who_left)):
		players_who_left.erase(info["name"])
		if(info["name"] == player_info[first_player]["name"]):
			first_abandoned_timer.stop()
			var temp = player_info[first_player]
			player_info.erase(first_player)
			set_first_player_id(id)
			player_info[first_player] = temp
		elif(info["name"] == player_info[second_player]["name"]):
			second_abandoned_timer.stop()
			var temp = player_info[second_player]
			player_info.erase(second_player)
			set_second_player_id(id)
			player_info[second_player] = temp
		
		
		send_board_info(id)
		if(players_who_left.size() == 0):
			current_phase = phase.PREP
		return
	
	
	player_info[id] = info
	
	if(player_info.size() == 1):
		set_first_player_id(id)
		player_info[first_player]["color"] = Global.colors.RED
	elif(player_info.size() == 2):
		set_second_player_id(id)
		player_info[second_player]["color"] = Global.colors.YELLOW
		network.send_board_info(first_player, player_info[second_player]["name"],
		Global.colors.RED)
		network.send_board_info(second_player, player_info[first_player]["name"],
		Global.colors.YELLOW)
		game_started = true
		
#	update_info_ui()


func set_first_player_id(_id):
	first_player = _id
	$"Battle Phase".first_player = _id
	$"Preparation Phase".first_player = _id

func set_second_player_id(_id):
	second_player = _id
	$"Battle Phase".second_player = _id
	$"Preparation Phase".second_player = _id

#Prep Phase Functions
func switch_to_battle_phase():
	current_phase = phase.BATTLE
	network.switch_to_battle_phase()
	get_node("Battle Phase").set_move(first_player)


func placed_a_piece(piece_type, coordinate):
	$"Preparation Phase".placed_a_piece(piece_type, coordinate)

func opponent_placed_a_piece(receiver, piece_type, coordinate):
	network.rpc_id(receiver, "opponent_placed_a_piece", piece_type, coordinate)

func players_turn(id):
	network.rpc_id(id, "players_turn")

### Battle Phase Functions ###
func move_performed(from, to):
	$"Battle Phase".move_performed(from, to)

func opponent_moved(receiver, from_adjusted, to_adjusted):
	network.rpc_id(receiver, "opponent_moved", from_adjusted, to_adjusted)

func set_time(receiver, first_timer, second_timer):
	network.rpc_id(receiver, "set_time", first_timer, second_timer)

func game_ends(winner):
	network.rpc_id(first_player, "game_ends", winner)
	network.rpc_id(second_player, "game_ends", winner)



