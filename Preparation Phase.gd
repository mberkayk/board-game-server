extends Node

# Player info, associate ID to data
var player_info = { }
var first_player:int = 0
var second_player:int = 0

var amt_of_pieces_on_the_board : int = 0

signal opponent_placed_a_piece
signal players_turn
signal all_pieces_placed

enum colors {RED, YELLOW}
enum pieces {DOE, DEER, BEAR, WALL}

onready var pieces_in_the_game = get_parent().pieces_in_the_game

onready var first_timer : Timer = Timer.new()
onready var second_timer : Timer = Timer.new()

func _ready():
	for _i in range(4):
		pieces_in_the_game.append(get_parent().Piece.new(Vector2(-1,-1), pieces.WALL, colors.RED))
		pieces_in_the_game.append(get_parent().Piece.new(Vector2(-1,-1), pieces.WALL, colors.YELLOW))
	
	for _i in range(2):
		pieces_in_the_game.append(get_parent().Piece.new(Vector2(-1,-1), pieces.DEER, colors.RED))
		pieces_in_the_game.append(get_parent().Piece.new(Vector2(-1,-1), pieces.DEER, colors.YELLOW))
		
		pieces_in_the_game.append(get_parent().Piece.new(Vector2(-1,-1), pieces.BEAR, colors.RED))
		pieces_in_the_game.append(get_parent().Piece.new(Vector2(-1,-1), pieces.BEAR, colors.YELLOW))
	
	pieces_in_the_game.append(get_parent().Piece.new(Vector2(-1,-1), pieces.DOE, colors.RED))
	pieces_in_the_game.append(get_parent().Piece.new(Vector2(-1,-1), pieces.DOE, colors.YELLOW))
	
	
	first_timer.one_shot = true
	first_timer.wait_time = 60;
	
	second_timer.one_shot = true
	second_timer.wait_time = 60;
	
	add_child(first_timer)
	add_child(second_timer)
	first_timer.start()


func placed_a_piece(piece_type, coordinate):
	var receiver : int
	var sender_color : int
	if(get_tree().get_rpc_sender_id() == first_player):
		first_timer.stop()
		second_timer.start()
		receiver = second_player
		sender_color = colors.RED
	elif(get_tree().get_rpc_sender_id() == second_player):
		second_timer.stop()
		first_timer.start()
		receiver = first_player
		sender_color = colors.YELLOW
	
	for piece in pieces_in_the_game:
		if(piece.tile_pos == Vector2(-1,-1) and piece.color == sender_color):
			piece.tile_pos = coordinate
	
	emit_signal("opponent_placed_a_piece", receiver, piece_type, coordinate)
	
	amt_of_pieces_on_the_board += 1
	if(amt_of_pieces_on_the_board == 18):
		emit_signal("all_pieces_placed")
		first_timer.stop()
		second_timer.stop()
	else:
		emit_signal("players_turn", receiver)
