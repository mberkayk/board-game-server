extends Node


onready var board_scene = load("res://Board.tscn")

onready var network = get_parent()

var boards = []
var players_boards = { }


func add_board():
	var board = board_scene.instance()
	board.construct(boards.size())
	boards.append(board)
	add_child(board)

func get_next_available_board():
	for board in boards:
		if (board.get_next_vacant_spot() < 2): return board
	#if the function hasn't returned by now
	add_board()
	return boards[boards.size()-1]

func add_player(id, info):
	var board = get_next_available_board()
	var spot = board.get_next_vacant_spot()
	if(spot == 0):
		board.first_player = id
	elif(spot == 1):
		board.second_player = id
	players_boards[id] = board.id
	
	board.register_player(id, info)

func get_board_by_player(player):
	return boards[players_boards[player]]
