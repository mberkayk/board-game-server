tool
extends EditorPlugin


var plugin : EditorPlugin
var x : Array

func _enter_tree():
	plugin = EditorPlugin.new()
	x = plugin.get_editor_interface().get_file_system_dock().get_children()
	for y in x:
		if(y.visible == false):
			x.erase(y)
	x[0].get_children()[1].get_children()[0].grab_focus()


func _input(event):
	if(event is InputEventWithModifiers and event.control and event.shift 
	and (event is InputEventKey) and event.pressed and event.get_scancode() == KEY_F):
		for y in x:
			if(y.visible == false):
				x.erase(y)
		x[0].get_children()[1].get_children()[0].grab_focus()

func _exit_tree():
	pass
