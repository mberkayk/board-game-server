extends Node

var labels : Array

onready var board_manager  = $"Board Manager"


func _ready():
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(7000, 20)
	get_tree().network_peer = peer
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")


func _player_disconnected(id):
	#TODO: write this function
	pass

### Pregame Remote Functions ###
func send_board_info(id, name, player_color,
		 player_time=null, opponent_time=null, 
		current_phase=null, tile_pos=null, type=null, color=null):
	
	#if only three parameters were given
	if(player_time == null):
		rpc_id(id, "set_board_info", name, player_color)
	else:
		rpc_id(id, "set_board_info", name,
			player_color, player_time, opponent_time, current_phase,
			tile_pos, type, color)

remote func register_player(info):
	var id = get_tree().get_rpc_sender_id()
	board_manager.add_player(id, info)


func switch_to_battle_phase():
	rpc("switch_to_battle_phase")


### Prep Phase Remote Functions ###
remote func placed_a_piece(piece_type, coordinate):
	var player = get_tree().get_rpc_sender_id()
	board_manager.get_board_by_player(player).placed_a_piece(piece_type, coordinate)

### Battle Phase Remote Functions ###
remote func move_performed(from, to):
	var player = get_tree().get_rpc_sender_id()
	board_manager.get_board_by_player(player).move_performed(from, to)
### ###


func update_info_ui():
	pass
#	var grid = get_node("GridContainer")
#	for label in labels:
#		grid.remove_child(label)
#	labels.clear()
#	for p in player_info:
#		var label = Label.new()
#		label.text = str(p)
#		grid.add_child(label)
#		labels.append(label)
#
#		label = Label.new()
#		label.text = str(player_info[p])
#		labels.append(label)
#		grid.add_child(label)
