extends Node

var first_player : int
var second_player : int

onready var first_timer : Timer = Timer.new()
onready var second_timer : Timer = Timer.new()

var whos_move:int = 0

onready var pieces_in_the_game = get_parent().pieces_in_the_game

signal opponent_moved
signal set_time

onready var board = get_parent()

func _ready():
	first_timer.one_shot = true
	first_timer.wait_time = 10 * 60;
	
	second_timer.one_shot = true
	second_timer.wait_time = 10 * 60;
	
	first_timer.connect("timeout", self, "first_player_timeout")
	second_timer.connect("timeout", self, "second_player_timeout")
	
	add_child(first_timer)
	add_child(second_timer)
	
	first_timer.set_paused(true)
	first_timer.start()
	second_timer.set_paused(true)
	second_timer.start()


func move_performed(from, to):
	#adjust values for the opponent
	var from_adjusted = Vector2(7,7) - from
	var to_adjusted = Vector2(7,7) - to
	
	var receiver : int
	if(get_tree().get_rpc_sender_id() == first_player):
		
		for piece in pieces_in_the_game:
			if(piece.tile_pos == from):
				piece.tile_pos = to
		
		receiver = second_player
		set_move(second_player)
	elif(get_tree().get_rpc_sender_id() == second_player):
		
		for piece in pieces_in_the_game:
			if(piece.tile_pos == from_adjusted):
				piece.tile_pos = to_adjusted
		
		receiver = first_player
		set_move(first_player)
	
	emit_signal("opponent_moved", receiver, from_adjusted, to_adjusted)
	board.set_time(receiver, first_timer.wait_time, second_timer.wait_time)


func set_move(player):
	if(player == first_player):
		first_timer.set_paused(false)
		second_timer.set_paused(true)
	elif(player == second_player):
		first_timer.set_paused(true)
		second_timer.set_paused(false)

func first_player_timeout():
	player_wins(get_parent().player_info[second_player]["name"])

func second_player_timeoout():
	player_wins(get_parent().player_info[first_player]["name"])


func player_wins(winner):
	board.game_ends(winner)
